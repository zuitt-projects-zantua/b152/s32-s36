const express = require('express');
const router = express.Router();

const courseControllers = require('../controllers/courseControllers')

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

router.post('/', verify, verifyAdmin, courseControllers.addCourse);
router.get('/', courseControllers.getAllCourses);
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);
router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);

router.put('/archive/:id', verify, verifyAdmin, courseControllers.archiveCourse);
router.put('/activate/:id', verify, verifyAdmin, courseControllers.activateCourse);
router.get('/getActiveCourses', courseControllers.getActiveCourses);

router.get('/getInactiveCourses', verify, verifyAdmin, courseControllers.getInactiveCourses)

router.post('/findCourseName', courseControllers.findCourseName)

router.post('/findCoursePrice', courseControllers.findCoursePrice)

router.get('/getEnrollees/:id', verify, verifyAdmin, courseControllers.getEnrollees)


/*
router.put('/isActiveCourse/:id', verify, verifyAdmin, courseControllers.isActiveCourse)
*/
/*
router.delete('/deleteCourse/:id', verify, verifyAdmin, courseControllers.deleteCourse)
*/


module.exports = router;