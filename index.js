const express = require('express');
const app = express();
const mongoose = require('mongoose');
const port = 4000;


mongoose.connect('mongodb+srv://angzang:angelo@cluster0.j7r8u.mongodb.net/bookingAPI152?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


//notifications for successful or failed mongodb connection
let db = mongoose.connection;
db.on('error',console.error.bind(console, "Connection Error"));
db.once('open',() => console.log("Connected to MongoDB"));


app.use(express.json());

const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')


app.use('/users', userRoutes);
app.use('/courses', courseRoutes)


app.listen(port, () => console.log(`Server is running at port ${port}`));

/*
	App: Booking System Api

	Description: Allows a user to enroll to a course. Allows an admin to do CRUD on courses. Allows us to register regular users

	User:
		firstName - string,
		lastName - string,
		email - string,
		password - string,
		mobileNo. - string,
		isAdmin - boolean,
			default- false,
		enrollments- [
			
			courseID - string,
			status - string,
			dateEnrolled - date

		]

	//Associative Entity - two way embedding
	Enrollment:
		userID - string,
		courseID - string,
		status - string,
		dateEnrolled - date

	Course:
		name - string,
		description - string,
		price - number,
		isActive - boolean
			default:true
		createdOn - date,
		enrollees - [
			
			userID - string,
			status - string,
			dateEnrolled - date
	
		]

*/