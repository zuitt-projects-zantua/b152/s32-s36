const User = require('../models/User');

//import course model
const Course = require('../models/Course');

const bcrypt = require('bcrypt');

const auth = require('../auth');
//console.log(auth);

module.exports.registerUser = (req,res) => {

	//console.log(req.body)

	const hashedPW = bcrypt.hashSync(req.body.password,10)
	//console.log(hashedPW)

	let newUser = new User({

		firstName : req.body.firstName,
		lastName : req.body.lastName,
		email : req.body.email,
		password : hashedPW,
		mobileNo : req.body.mobileNo
	})

	newUser.save()
	.then(user => res.send(user))
	.catch(error => res.send(error))

}

module.exports.getAllUsers = (req,res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res) => {

	//console.log(req.body)

	User.findOne({email: req.body.email})
	.then(result => {

		//console.log(result)
		if(result === null){
			return res.send("No user Found.")
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password)
			console.log(isPasswordCorrect);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(result)})
			}
			else{
				return res.send("Incorrect Password.")
			}
		}
	})
	.catch(error => res.send(error))
}


module.exports.getUserDetails = (req,res) => {

	//console.log(req.user)

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
	//res.send("Testing for verify")
}

module.exports.checkEmailExists = (req,res) => {

	//console.log(req.body)

	//User.findOne({email: {$regex: req.body.email, $options: 'i'}})
	
	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
			return res.send("Email is available.")
		}
		else{
			return res.send("Email is already registered.")
		}
	})
	.catch(error => res.send(error))
}


module.exports.updateUserDetails = (req,res) => {

	//console.log(req.user.id)
	//console.log(req.body)

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id,updates,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.updateAdmin = (req,res) => {

	//console.log(req.user.id)

	User.findByIdAndUpdate(req.params.id, {isAdmin: true}, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


module.exports.enroll = async (req,res) => {

	//console.log(req.user.id)
	//console.log(req.body.courseId)
	//console.log(req.user.isAdmin)

	if(req.user.isAdmin){
		return res.send("Action forbidden")
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {

		//console.log(user);//user

		let newEnrollment = {
			courseId: req.body.courseId
		}

		user.enrollments.push(newEnrollment);

		return user.save().then(user => true).catch(error => error.message)
	})

	//console.log(isUserUpdated)

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(error => error.message)
	})

	//console.log(isCourseUpdated)

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}
}

module.exports.getEnrollments = (req,res) =>{

	//console.log(req.user.id)

	User.findById(req.user.id)
	.then(user => {

		let enrolledCourses = user.enrollments
		
		if(enrolledCourses.length === 0){
			return res.send({message: "You are not enrolled in any courses."})
		}
		else{
			return res.send(enrolledCourses)
		}
	})
	.catch(error => res.send(error))
}

/*
module.exports.deleteUser = (req,res) => {

	console.log(req.user.id)

	User.deleteOne({_id: req.user.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
*/