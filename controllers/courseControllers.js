const Course = require('../models/Course');

module.exports.addCourse = (req,res) => {
	console.log(req.body)

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newCourse.save()
	.then(course => res.send(course))
	.catch(error => res.send(error))
}

module.exports.getAllCourses = (req,res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getSingleCourse = (req,res) => {

	//console.log(req.params)

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.updateCourse = (req,res) => {

	//console.log(req.params.id)
	//console.log(req.body)

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
		//isActive: req.body.isActive
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.archiveCourse = (req,res) => {
	
	//console.log(req.params.id)

	Course.findByIdAndUpdate(req.params.id, {isActive: false}, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.activateCourse = (req,res) => {
	
	//console.log(req.params.id)

	Course.findByIdAndUpdate(req.params.id, {isActive: true}, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


module.exports.getInactiveCourses = (req,res) => {

	//console.log("hello")

	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.findCourseName = (req,res) => {

	//console.log(req.body)

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send("No course found.")
		}
		else{
			console.log(result)
			return res.send(result)
		}
	})
	.catch(error => res.send(error))
}


module.exports.findCoursePrice = (req,res) => {

	Course.find({price: req.body.price})
	.then(result => {
		if(result.length === 0){
			return res.send("No course found.")
		}
		else{
			//console.log(result)// [] - result.length === 0
			return res.send(result)
		}
	})
	.catch(error => res.send(error))
}



module.exports.getEnrollees = (req,res) => {

	//console.log(req.params.id)

	Course.findById(req.params.id)
	.then(course => {

		let enrolledUsers = course.enrollees
		
		if(enrolledUsers.length === 0){
			return res.send({message: "No enrollees."})
		}
		else{
			return res.send(enrolledUsers)
		}
	})
	.catch(error => res.send(error))
}




/*
module.exports.isActiveCourse = (req,res) => {

	//console.log(req.params)

	Course.findById(req.params.id)
	.then(course => {

		course.isActive = !course.isActive
		res.send(course)
	})
	.catch(error => res.send(error))
}
*/
/*
module.exports.deleteCourse = (req,res) => {

	//console.log(req.params)

	Course.deleteOne({_id: req.params.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
*/
