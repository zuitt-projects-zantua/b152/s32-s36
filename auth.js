const jwt = require("jsonwebtoken");
const secret = "CourseBookingApi"



module.exports.createAccessToken = (user) => {

	//console.log(user)

	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//console.log(data)

	return jwt.sign(data,secret,{});
}

module.exports.verify = (req,res,next) => {

	//console.log(req.headers.authorization)
	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No token."})
	}
	else {

		//console.log(token)//token before slice
		token = token.slice(7,token.length)
		//console.log(token)//token after slice

		jwt.verify(token, secret, (error,decodedToken) =>{

			if(error){
				return res.send({
					auth:"failed",
					message: error.message
				})
			}
			else {
				//console.log(decodedToken);
				req.user = decodedToken;

				next();
			}
		})

	}
}

module.exports.verifyAdmin = (req,res,next) => {

	//console.log(req.user);

	if(req.user.isAdmin){
		next();
	}
	else{
		res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}