const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({

	//required field
	//default field
	name: {
		type:String,
		required: [true, "Name is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required."]
			},
			status: {
				type: String,
				default: "Enrolled"
			},
			dateEnrolled: {
				type: String,
				default: "Enrolled"
			}
		}
	]

})

module.exports = mongoose.model('Course', courseSchema);